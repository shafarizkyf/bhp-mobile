import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import mystyles from '../config/mystyles';
import colors from '../config/colors';

const Avatars = (props) => (
  <View style={[mystyles.row]}>
    { props.avatars && props.avatars.map((avatar, index) => <Image key={index} source={avatar.image} style={[styles.avatar]} />) }
  </View>
)

const CardSimple = (props) => (
  <View style={[styles.card]}>
    <View style={[styles.cardHeader, mystyles.row, mystyles.spaceBetween]}>
      <Text style={[mystyles.textWhite]}>{props.textLeft}</Text>
      { props.textRight && <Text style={[mystyles.textWhite, mystyles.bold]}>{props.textRight}</Text> }
    </View>
    <View style={[styles.cardBody]}>
      { props.textDescription && <Text style={[]}>{props.textDescription}</Text> }
      { props.avatars && <Avatars avatars={props.avatars} />}
    </View>
  </View>
);


const styles = StyleSheet.create({
  card:{
    marginBottom: 10,
    borderRadius: 3,
    backgroundColor: 'white'
  },
  cardHeader:{
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    backgroundColor: colors.softBlue,
    paddingHorizontal: 20,
    paddingVertical: 5
  },
  cardBody:{
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  avatar:{
    width: 40,
    height: 40,
    borderRadius: 20,
    marginHorizontal: -5,
    borderColor: 'white',
    borderWidth: 2,
  }
});

export default CardSimple;