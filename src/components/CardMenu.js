import React from 'react';
import { 
  View, 
  Text, 
  Image, 
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import mystyles from '../config/mystyles';

const CardMenu = (props) => (
  <TouchableOpacity activeOpacity={0.8} style={[styles.card]} onPress={props.page}>
    <View style={[mystyles.row]}>
      <Image source={props.icon} style={[mystyles.icon50]} />
      <Text style={[styles.cardText]}>{props.title}</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  card:{
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderRadius: 3,
    marginBottom: 10,
  },
  cardText:{
    fontSize: 18,
    textAlignVertical: 'center',
    marginLeft: 10,
  }
});

export default CardMenu;