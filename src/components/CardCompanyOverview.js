import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import mystyles from '../config/mystyles';
import colors from '../config/colors';

const CardCompanyOverview = (props) => (
  <View>
    <View style={[styles.cardHeader]}>
      <Text style={[styles.cardHeaderTitle]}>{props.title}</Text>
    </View>
    <View style={[styles.cardBody]}>
      <View style={[mystyles.row, mystyles.spaceBetween]}>
        {
          props.dataBody.map((obj, index) => (
            <View key={index}>
              <Text style={[mystyles.f10, mystyles.textGrey]}>{obj.label}</Text>
              <Text style={[mystyles.textOrange, mystyles.f14, mystyles.bold]}>{obj.value}</Text>
            </View>
          ))
        }
      </View>
      <View style={[mystyles.separator]}></View>
      <View style={[mystyles.row, mystyles.spaceBetween]}>
        {
          props.dataFooter.map((obj, index) => (
            <View key={index}>
              <Text style={[mystyles.f10, mystyles.textGrey]}>{obj.label}</Text>
              <Text style={[mystyles.f12, mystyles.bold, mystyles.textGrey]}>{obj.value}</Text>
            </View>
          ))
        }
      </View>
    </View>
  </View>
);


const styles = StyleSheet.create({
  cardHeader:{
    backgroundColor: colors.orange,
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  cardHeaderTitle:{
    color: 'white',
    fontWeight: 'bold'
  },
  cardBody:{
    padding: 20,
    backgroundColor: 'white',
    marginBottom: 10,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10
  }
});

export default CardCompanyOverview;