import React from 'react';
import {
  View, 
  Text, 
  Image,
  TouchableOpacity, 
  StyleSheet,
} from 'react-native';
import colors from '../config/colors';
import mystyles from '../config/mystyles';

const Navbar = (props) => {
  return (
    <View style={{ height:50, backgroundColor:colors.navy }}>
      <View style={ styles.navRow }>
        <TouchableOpacity onPress={ props.openDrawer }>
          <Image source={ require('../../assets/icons/menu.png') } style={[mystyles.icon20, mystyles.mr20]} />
        </TouchableOpacity>
        <Text style={[mystyles.f16, mystyles.textWhite, mystyles.bold]}>{ props.title }</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  navRow:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
  },
  notification:{
    flex:1,
    alignItems:'flex-end', 
  }
});

export default Navbar;