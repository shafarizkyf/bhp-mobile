import React from 'react';
import { 
  View, 
  Text, 
  Image, 
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import mystyles from '../config/mystyles';
import colors from '../config/colors';

const CardList = (props) => (
  <TouchableOpacity activeOpacity={0.8} style={[styles.card]} onPress={props.page} >
    <View style={[mystyles.row, mystyles.spaceBetween]}>
      <View style={[styles.cardTextHeader]}>
        <Text style={[styles.title]}>{ props.title }</Text>
        <View style={[mystyles.row, mystyles.spaceBetween]}>
          <Text style={[mystyles.bold, mystyles.f12, mystyles.textGrey]}>{ props.category }</Text>
          <Text style={[styles.label, mystyles.bgBlue, mystyles.ml10]}>{ props.label }</Text>
        </View>
      </View>
      <Image source={ props.icon } style={[mystyles.icon50]} />
    </View>
    {
      props.data &&
      <View style={[mystyles.separator]}></View>
    }
    <View style={[mystyles.row, mystyles.spaceBetween]}>
      {
        props.data && props.data.map((obj, index) => (
          <View key={index}>
            <Text style={[mystyles.textGrey, mystyles.f10]}>{ obj.label }</Text>
            <Text style={[mystyles.textGrey]}>{ obj.value }</Text>
          </View>
        ))
      }
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  card:{
    backgroundColor:'white',
    padding: 20,
    borderRadius: 3,
    marginBottom: 10
  },
  title:{
    fontSize: 20,
    color: colors.black
  },
  cardTextHeader:{
    justifyContent: 'center', 
    width: '70%'
  },
  label:{
    paddingHorizontal: 10,
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold'
  },
});

export default CardList;