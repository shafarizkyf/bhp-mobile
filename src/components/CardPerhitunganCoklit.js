import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import mystyles from '../config/mystyles';
import colors from '../config/colors';

const CardPerhitunganCoklit = (props) => (
  <View style={[styles.card]}>
    <View style={[styles.cardHeader, mystyles.row, mystyles.spaceBetween]}>
      <Text style={[mystyles.bold, mystyles.textWhite]}>{props.data.jenis}</Text>
      <Text style={[mystyles.bold, mystyles.textWhite]}>{props.data.total}</Text>
    </View>
    <View style={[styles.cardBody]}>
      {
        props.data.komponen.map((obj,index) => (
          <View key={index} style={[mystyles.row, mystyles.mb10, mystyles.spaceBetween]}>
            <Text style={[]}>{obj.nama}</Text>
            <Text style={[]}>{obj.total}</Text>
          </View>
        ))
      }
    </View>
    <View style={[styles.cardFooter]}>
      <Text style={[mystyles.textCenter, mystyles.textDarkGrey, mystyles.f12]}>Self Asessement: {props.data.nilaiSelfAsessement}</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  card:{
    borderRadius: 3,
    backgroundColor: 'white',
    marginBottom: 10
  },
  cardHeader:{
    backgroundColor: colors.orange,
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },
  cardBody:{
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  cardFooter:{
    backgroundColor: colors.lightGrey,
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
  }
});

export default CardPerhitunganCoklit;