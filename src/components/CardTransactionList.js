import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';
import mystyles from '../config/mystyles';

const CardTransactionList = (props) => (
  <View style={[styles.card]}>
    <View style={[mystyles.row, mystyles.spaceBetween]}>
      <View style={[mystyles.row]}>
        <Image source={props.icon} style={[mystyles.icon50]} />
        <View style={[mystyles.ml10, mystyles.justifyCenter]}>
          <Text style={[mystyles.f10, mystyles.textGrey]}>{props.date1}</Text>
          <Text style={[mystyles.f14, mystyles.textOrange, mystyles.bold]}>{props.subject}</Text>
          <Text style={[mystyles.f10, mystyles.textGrey, mystyles.italic]}>{props.date2}</Text>
        </View>
      </View>
      <View style={[mystyles.justifyCenter]}>
        { props.amountTitle && <Text style={[mystyles.f10, mystyles.textGrey]}>{props.amountTitle}</Text>}
        <Text style={[mystyles.textOrange, mystyles.bold, mystyles.f14]}>{props.amount}</Text>
      </View>
    </View>
    { props.dataFooter && <View style={[mystyles.separator]}></View> }
    <View style={[mystyles.row, mystyles.spaceBetween]}>
      {
        props.dataFooter && props.dataFooter.map((obj, index) => (
          <View key={index}>
            <Text style={[mystyles.f10, mystyles.textGrey]}>{obj.label}</Text>
            <Text style={[mystyles.f12, mystyles.bold, mystyles.textGrey]}>{obj.value}</Text>
          </View>
        ))
      }    
    </View>
  </View>
);

const styles = StyleSheet.create({
  card:{
    backgroundColor: 'white',
    padding: 15,
    marginBottom: 10,
    borderRadius: 5,
    marginBottom: 10
  }
})

export default CardTransactionList;