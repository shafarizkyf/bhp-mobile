import React from 'react';
import { View, StyleSheet } from 'react-native';
import Timeline from 'react-native-timeline-listview';
import colors from '../config/colors';

const MyTimeline = (props) =>  (
  <View style={[styles.card]}>
    <Timeline
      circleColor={colors.softBlue}
      lineColor={colors.softBlue} 
      innerCircle="dot"
      timeStyle={styles.timeStyle}
      titleStyle={styles.titleStyle}
      descriptionStyle={styles.descriptionStyle}
      data={props.data} />
  </View>
);

const styles = StyleSheet.create({
  card:{
    backgroundColor:'white',
    padding: 20,
    borderRadius: 5,
    marginBottom: 10
  },
  timeStyle:{
    backgroundColor: colors.softBlue,
    color: 'white',
    paddingVertical: 3,
    paddingHorizontal: 5,
    borderRadius: 3,
    fontSize: 12
  },
  titleStyle:{
    color: colors.orange,
    marginTop: 0,
    marginBottom: 5
  },
  descriptionStyle:{
    color: colors.grey,
    marginTop: 0
  }
});

export default MyTimeline;