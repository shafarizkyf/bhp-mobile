import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import mystyles from '../config/mystyles';
import colors from '../config/colors';

const CardSurat = (props) => (
  <View style={[styles.card]}>
    <View style={[mystyles.row]}>
      <View style={[mystyles.justifyCenter]}>
        <Image source={ require('../../assets/icons/bill.png') } style={[mystyles.icon50]} />
      </View>
      <View style={[mystyles.justifyCenter, mystyles.ml10]}>
        <View style={[mystyles.mb10]}>
          <Text style={[mystyles.f10, mystyles.textGrey]}>{props.label}</Text>
          <Text style={[mystyles.f14, mystyles.textOrange, mystyles.bold]}>{props.value}</Text>
          <Text style={[mystyles.f10, mystyles.textGrey]}>{props.status}</Text>
        </View>
        <View style={[mystyles.row]}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => props.download(props.index)}>
            <Text style={[styles.button, mystyles.bgSoftBlue]}>Download</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.8} onPress={() => props.preview(props.index)}>
            <Text style={[styles.button, mystyles.bgOrange]}>Preview</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
    { props.verification && <View style={[mystyles.separator]}></View> }
    {
      props.verification &&
      <View style={[styles.buttonRight]}>
        <View style={[mystyles.row]}>
          <TouchableOpacity activeOpacity={0.8} onPress={() => props.onVerification(false, props.index)}>
            <Text style={[styles.buttonReject, props.status == 'Tolak' ? styles.borderSelected : {}]}>Tolak</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.8} onPress={() => props.onVerification(true, props.index)}>
            <Text style={[styles.buttonAccept, props.status == 'Terima' ? styles.borderSelected : {}]}>Terima</Text>
          </TouchableOpacity>
        </View>
      </View> 
    }
  </View>
);

const styles = StyleSheet.create({
  card:{
    backgroundColor:'white',
    padding: 20,
    borderRadius: 5,
    marginBottom: 10
  },
  button:{
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginRight: 10,
    borderRadius: 3
  },
  buttonReject:{
    fontSize: 14,
    fontWeight: 'bold',
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginRight: 10,
    borderRadius: 3,
    color: 'white',
    backgroundColor: colors.grey,
  },
  buttonAccept:{
    fontSize: 14,
    fontWeight: 'bold',
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginRight: 10,
    borderRadius: 3,
    color: 'white',
    backgroundColor: colors.green,
  },  
  borderSelected:{
    backgroundColor: colors.navy
  },
  buttonRight:{
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  }
});

export default CardSurat;