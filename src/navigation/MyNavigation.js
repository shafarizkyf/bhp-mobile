import { 
  createAppContainer, 
  createDrawerNavigator,
  createSwitchNavigator,
} from 'react-navigation';
import { 
  Dashboard, 
  Laporan, 
  Surat,
  Profile,
  VerifikasiDokumen,
  Coklit,
} from '../pages';
import Login from '../pages/Login';
import MyStackNavigator from './MyStackNavigator';

const MainNavigator = createDrawerNavigator({
  dashboard:{
    screen: Dashboard,
    navigationOptions:{
      title: 'Dashboard'
    }
  },
  verifikasiDokumen:{
    screen: VerifikasiDokumen,
    navigationOptions:{
      title: 'Verifikasi Dokumen'
    }
  },
  coklit:{
    screen: Coklit,
    navigationOptions:{
      title: 'Pencocokan dan Penelitian'
    }
  },
  surat:{
    screen: Surat,
    navigationOptions:{
      title: 'Surat'
    }  
  },
  laporan:{
    screen: Laporan,
    navigationOptions:{
      title: 'Laporan'
    }
  },
  profile:{
    screen: Profile,
    navigationOptions:{
      title: 'Profile'
    }
  },
  stacks:{
    screen: MyStackNavigator,
    navigationOptions:{
      drawerLabel: () => null
    }
  },
}, {
  initialRouteName: 'dashboard'
});

const AppNavigator = createSwitchNavigator({
  login: Login,
  main: MainNavigator
})

const AppContainer = createAppContainer(AppNavigator)

export default AppContainer;