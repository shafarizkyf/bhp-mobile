import { createStackNavigator } from 'react-navigation';
import { 
  LaporanPiutang, 
  LaporanTagihan, 
  LaporanWajibBayar, 
  SuratPemberitahuan, 
  SuratPemberitahuanDetail, 
  SuratTagihan, 
  SuratTagihanDetail, 
  SuratHasilCoklit, 
  Menu,
  VerifikasiDokumenDetail,
  PreviewDokumen,
} from '../pages';
import SuratTeguranPembayaranDetailNavigator from './surat/SuratTeguranPembayaranDetailNavigator';
import SuratTeguranDokumenDetailNavigator from './surat/SuratTeguranDokumenDetailNavigator';
import SuratTeguranNavigator from './surat/SuratTeguranNavigator';
import LaporanPiutangDetailNavigator from './laporan/LaporanPiutangDetailNavigator';
import LaporanTagihanDetailNavigator from './laporan/LaporanTagihanDetailNavigator';
import colors from '../config/colors';
import CoklitDetailNavigator from './coklit/CoklitDetailNavigator';

const MyStackNavigator = createStackNavigator({
  menu:{
    screen: Menu,
    navigationOptions:{
      title: 'Menu',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  verifikasiDokumenDetail:{
    screen: VerifikasiDokumenDetail,
    navigationOptions:{
      title: 'Verifikasi Dokumen',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  laporanPiutang:{
    screen: LaporanPiutang,
    navigationOptions:{
      title: 'Laporan Piutang',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  laporanPiutangDetail:{
    screen: LaporanPiutangDetailNavigator,
    navigationOptions:{
      title: 'Laporan Piutang Detail',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  laporanTagihan:{
    screen: LaporanTagihan,
    navigationOptions:{
      title: 'Laporan Tagihan',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  laporanTagihanDetail:{
    screen: LaporanTagihanDetailNavigator,
    navigationOptions:{
      title: 'Tagihan Detail',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  laporanWajibBayar:{
    screen: LaporanWajibBayar,
    navigationOptions:{
      title: 'Laporan Wajib Bayar',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  suratPemberitahuan:{
    screen: SuratPemberitahuan,
    navigationOptions:{
      title: 'Surat Pemberitahuan',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  suratPemberitahuanDetail:{
    screen: SuratPemberitahuanDetail,
    navigationOptions:{
      title: 'Surat Pemberitahuan Detail',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  suratTagihan:{
    screen: SuratTagihan,
    navigationOptions:{
      title: 'Surat Tagihan',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  suratTagihanDetail:{
    screen: SuratTagihanDetail,
    navigationOptions:{
      title: 'Surat Tagihan Detail',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  suratTeguran:{
    screen: SuratTeguranNavigator,
    navigationOptions:{
      title: 'Surat Teguran',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  suratTeguranPembayaranDetailAll:{
    screen: SuratTeguranPembayaranDetailNavigator,
    navigationOptions:{
      title: 'Surat Teguran Dokumen Detail',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  suratTeguranDokumenDetailAll:{
    screen: SuratTeguranDokumenDetailNavigator,
    navigationOptions:{
      title: 'Surat Teguran Dokumen Detail',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  suratHasilCoklit:{
    screen: SuratHasilCoklit,
    navigationOptions:{
      title: 'Surat Hasil Coklit',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  previewDokumen:{
    screen: PreviewDokumen,
    navigationOptions:{
      title: 'Preview Dokumen',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
  coklitDetail:{
    screen: CoklitDetailNavigator,
    navigationOptions:{
      title: 'Detail Coklit',
      headerStyle: { backgroundColor: colors.navy },
      headerTitleStyle: { color: '#fff' },
      headerTintColor: '#fff'
    }
  },
}, {
  initialRouteName: 'menu'
});

export default MyStackNavigator;