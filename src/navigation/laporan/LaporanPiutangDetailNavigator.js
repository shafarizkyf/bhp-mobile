import { createMaterialTopTabNavigator } from 'react-navigation';
import { LaporanPiutangPnbp, LaporanPiutangDenda } from '../../pages';
import colors from '../../config/colors';

const tabBarOptions = {
  style:{
    backgroundColor: colors.navy
  },
}

const LaporanPiutangDetailNavigator = createMaterialTopTabNavigator({
  laporanPiutangPnbp:{
    screen: LaporanPiutangPnbp,
    navigationOptions:{
      title: 'Detail PNBP'
    }
  },
  laporanPiutangRincian:{
    screen: LaporanPiutangDenda,
    navigationOptions:{
      title: 'Rincian Denda'
    }
  }
},{ tabBarOptions });

export default LaporanPiutangDetailNavigator;