import { createMaterialTopTabNavigator } from 'react-navigation';
import { LaporanTagihanDetail, LaporanTagihanCoklit } from '../../pages';
import colors from '../../config/colors';

const tabBarOptions = {
  style:{
    backgroundColor: colors.navy
  },
}

const LaporanTagihanDetailNavigator = createMaterialTopTabNavigator({
  laporanTagihanDetail:{
    screen: LaporanTagihanDetail,
    navigationOptions:{
      title: 'Detail Tagihan'
    }
  },
  laporanTagihanCoklit:{
    screen: LaporanTagihanCoklit,
    navigationOptions:{
      title: 'Hasil Coklit'
    }
  }
},{ tabBarOptions });

export default LaporanTagihanDetailNavigator;