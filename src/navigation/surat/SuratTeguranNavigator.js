import { createMaterialTopTabNavigator } from 'react-navigation';
import { SuratTeguranPembayaran, SuratTeguranDokumen } from "../../pages";
import colors from '../../config/colors';

const tabBarOptions = {
  style:{
    backgroundColor: colors.navy
  },
}

const SuratTeguranNavigator = createMaterialTopTabNavigator({
  suratTeguranPembayaran:{
    screen: SuratTeguranPembayaran,
    navigationOptions:{
      title: 'Teguran Pembayaran'
    }
  },
  suratTeguranDokumen:{
    screen: SuratTeguranDokumen,
    navigationOptions:{
      title: 'Teguran Dokumen'
    }
  },
},{ tabBarOptions });

export default SuratTeguranNavigator;