import { createMaterialTopTabNavigator } from 'react-navigation';
import { SuratTeguranDokumenDetail, SuratTeguranDokumenListDokumen } from '../../pages';
import colors from '../../config/colors';

const tabBarOptions = {
  style:{
    backgroundColor: colors.navy
  },
}

const SuratTeguranDokumenDetailNavigator = createMaterialTopTabNavigator({
  suratTeguranDokumenDetail:{
    screen: SuratTeguranDokumenDetail,
    navigationOptions:{
      title: 'Validasi',
      drawerLabel: () => null
    }
  },
  suratTeguranDokumenListDokumen:{
    screen: SuratTeguranDokumenListDokumen,
    navigationOptions:{
      title: 'List Dokumen',
      drawerLabel: () => null
    }
  }
}, { tabBarOptions });

export default SuratTeguranDokumenDetailNavigator;