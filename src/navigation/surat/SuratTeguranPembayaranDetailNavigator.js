import { createMaterialTopTabNavigator } from 'react-navigation';
import { SuratTeguranPembayaranDetail, SuratTeguranSelfAsessement } from '../../pages';
import colors from '../../config/colors';

const tabBarOptions = {
  style:{
    backgroundColor: colors.navy
  },
}

const SuratTeguranPembayaranDetailNavigator = createMaterialTopTabNavigator({
  suratTeguranPembayaranDetail:{
    screen: SuratTeguranPembayaranDetail,
    navigationOptions:{
      title: 'Validasi',
      drawerLabel: () => null
    }
  },
  suratTeguranSelfAsessement:{
    screen: SuratTeguranSelfAsessement,
    navigationOptions:{
      title: 'Self Asessement',
      drawerLabel: () => null
    }
  }
}, { tabBarOptions });

export default SuratTeguranPembayaranDetailNavigator;