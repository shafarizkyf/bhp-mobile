import { createMaterialTopTabNavigator } from 'react-navigation';
import { PerhitunganCoklit, TagihanCoklit, KesepakatanCoklit } from '../../pages';
import colors from '../../config/colors';

const tabBarOptions = {
  style:{
    backgroundColor: colors.navy
  },
}

const CoklitDetailNavigator = createMaterialTopTabNavigator({
  perhitunganCoklit:{
    screen: PerhitunganCoklit,
    navigationOptions:{
      title: 'Perhitungan'
    }
  },
  tagihanCoklit:{
    screen: TagihanCoklit,
    navigationOptions:{
      title: 'Tagihan'
    }
  },
  kesepakatanCoklit:{
    screen: KesepakatanCoklit,
    navigationOptions:{
      title: 'Kesepakatan'
    }
  },
}, { tabBarOptions });

export default CoklitDetailNavigator;