import React from 'react';
import { View, ScrollView } from 'react-native';
import { CardPerhitunganCoklit } from '../../components';
import mystyles from '../../config/mystyles';

export default class PerhitunganCoklit extends React.Component{

  state={
    dataCoklit: [
      {
        jenis: 'Pendapatan Kotor',
        total: '21.000.000.000',
        komponen:[
          { nama: 'Pendapatan A', total: '10.000.000.000' },
          { nama: 'Pendapatan B', total: '11.000.000.000' }
        ],
        nilaiSelfAsessement: '20.000.000.000'
      },
      {
        jenis: 'Pendapatan Diluar Telekomunikasi',
        total: '0',
        komponen:[],
        nilaiSelfAsessement: '0'
      },
      {
        jenis: 'Total Pendapatan',
        total: '21.000.000.000',
        komponen:[],
        nilaiSelfAsessement: '20.000.000.000'
      },
      {
        jenis: 'Biaya Interkoneksi',
        total: '0',
        komponen:[],
        nilaiSelfAsessement: '0'
      },
      {
        jenis: 'Write Off',
        total: '0',
        komponen:[],
        nilaiSelfAsessement: '0'
      },
      {
        jenis: 'Faktor Pengurang',
        total: '0',
        komponen:[],
        nilaiSelfAsessement: '0'
      },
    ]
  }

  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mh20]}>
            <View style={[mystyles.mt20]}></View>
            { this.state.dataCoklit.map((obj, index) => <CardPerhitunganCoklit key={index} data={obj} /> ) }
          </View>
        </ScrollView>        
      </View>
    );
  }
}

