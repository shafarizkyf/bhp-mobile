import React from 'react';
import { View, ScrollView} from 'react-native';
import { Navbar, CardList } from '../../components';
import mystyles from '../../config/mystyles';

export default class Coklit extends React.Component{

  state =  {
    data:[
      {
        wajibBayar: 'PT. TELKOM',
        tahun: '2018',
        selfAsessement: '20.500.000.000',
      },
      {
        wajibBayar: 'PT. INDOSAT',
        tahun: '2017',
        selfAsessement: '17.000.000.000',
      },
      {
        wajibBayar: 'PT. 3 IS THREE',
        tahun: '2017',
        selfAsessement: '21.500.000.000',
      },
    ]
  }

  render(){
    return(
      <View style={[mystyles.page]}>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } title="Pencocokan dan Penelitian" />
        <View style={[mystyles.page, mystyles.bgBlue]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>
            {
              this.state.data.map((obj, index) => (
                <CardList
                  key={index}
                  title={ obj.wajibBayar }
                  category={ obj.tahun }
                  label={ obj.selfAsessement } 
                  icon={ require('../../../assets/icons/insignia.png') }
                  page={ () => this.props.navigation.navigate('perhitunganCoklit') } 
                />
              ))
            }
          </ScrollView>
        </View>
      </View>
    );
  }
}