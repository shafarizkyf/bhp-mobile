import React from 'react';
import { View, ScrollView } from 'react-native';
import { CardSimple, CardTransactionList } from '../../components';
import mystyles from '../../config/mystyles';

export default class KesepatanCoklit extends React.Component{

  state={
    dataTagihan: [
      {
        jenisPembayaran: 'BHP TEL',
        tanggal: '30 April 2018',
        denda: '824.322',
        hasilCoklit: '100.000.000',
        terhutang: '10.824.322',
        terbayar: '90.000.000'
      },
      {
        jenisPembayaran: 'KPU',
        tanggal: '30 April 2018',
        denda: '3.122.424',
        hasilCoklit: '200.000.000',
        terhutang: '33.122.424',
        terbayar: '170.000.000'
      },
    ],
    timCoklit:[
      { id:1, image: require('../../../assets/users/1.jpg') },
      { id:2, image: require('../../../assets/users/2.jpg') },
      { id:3, image: require('../../../assets/users/3.jpg') },
      { id:4, image: require('../../../assets/users/4.jpg') },
      { id:5, image: require('../../../assets/users/5.jpg') },
    ],
    timWajibBayar:[
      { id:1, image: require('../../../assets/users/6.jpg') },
      { id:2, image: require('../../../assets/users/7.jpg') },
      { id:3, image: require('../../../assets/users/8.jpg') },
      { id:4, image: require('../../../assets/users/9.jpg') },
      { id:5, image: require('../../../assets/users/10.jpg') },
    ],
  }

  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mh20]}>
            <View style={[mystyles.mt20]}></View>

            <CardSimple 
              textLeft="Metode Kesepakatan"
              textRight="Langsung"
              textDescription="Dari hasil perhitungan dibawah ini, maka tim coklit, dan PT.Telkim selaku Wajib Bayar, menyatakan kesepakatan" 
            />

            {
              this.state.dataTagihan.map((obj, index) => (
                <CardTransactionList 
                  key={index}
                  icon={ require('../../../assets/icons/money.png') }
                  date1={ obj.tanggal }
                  subject={ obj.jenisPembayaran }
                  amount={ obj.terhutang }
                  amountTitle="Kurang Bayar"
                  dataFooter={[
                    { label:'Terbayar', value:obj.terbayar },
                    { label:'Hasil Coklit', value:obj.hasilCoklit },
                    { label:'Denda', value:obj.denda },
                  ]} 
                />
              ))
            }

            <CardSimple 
              textLeft="Tim Coklit"
              avatars={this.state.timCoklit}
            />

            <CardSimple 
              textLeft="PT. Reksa"
              avatars={this.state.timWajibBayar}
            />

          </View>
        </ScrollView>
      </View>
    );
  }
}