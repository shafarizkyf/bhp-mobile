import React from 'react';
import { 
  View, 
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground, 
  StyleSheet 
} from 'react-native';
import mystyles from '../config/mystyles';
import colors from '../config/colors';
import validate from 'validate.js';
import loginRule from '../validation/LoginRule';
import Axios from 'axios';
import api from '../config/api';

export default class Login extends React.Component{

  state = {
    errors: []
  }

  login(){
    const errors = validate(this.state, loginRule);
    if(errors){
      this.setState({errors});

    }else{
      Axios.post(api.baseUrl + api.login).then(response => {
        console.log('login -->', response);
      }).catch(error => {
        console.log('error login -->', error);
      });

      this.props.navigation.navigate('main');
    }
  }

  render(){
    return(
      <View style={ mystyles.page }>
        <ImageBackground source={ require('../../assets/images/bg.jpg') } style={mystyles.loginBg} />
        <View style={mystyles.loginBgColor}></View>
        <View style={[mystyles.page]}>
          <View style={[styles.wrapper]}>
            <TextInput
              onChangeText={(username) => this.setState({username})}
              style={[styles.textInput]}
              placeholder="username"
              placeholderTextColor="#fff"
              autoCapitalize="none"
              underlineColorAndroid="rgba(0,0,0,0)"
               />
            { 
              this.state.errors.username &&
              <Text style={[mystyles.textWhite, mystyles.italic, mystyles.f10]}>{this.state.errors.username}</Text>
            }

            <TextInput
              onChangeText={(password) => this.setState({password})}
              style={[styles.textInput]}
              placeholder="password"
              placeholderTextColor="#fff"
              autoCapitalize="none"
              underlineColorAndroid="rgba(0,0,0,0)"
              secureTextEntry={true} />
            { 
              this.state.errors.password &&
              <Text style={[mystyles.textWhite, mystyles.italic, mystyles.f10]}>{this.state.errors.password}</Text>
            }

            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.login() }>
              <Text style={[styles.button]}>LOGIN</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput:{
    marginBottom: 10,
    color: 'white',
    paddingHorizontal: 10,
    paddingBottom: 10,
    fontSize: 16,
    borderBottomWidth: 1,
    borderBottomColor: 'white'
  },
  button:{
    backgroundColor: colors.orange,
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 13,
    textAlign: 'center',
    borderRadius: 3,
    marginTop: 20
  },
  wrapper:{
    marginHorizontal: 40,
    justifyContent: 'flex-end',
    flex: 1,
    marginBottom: 80
  }
});