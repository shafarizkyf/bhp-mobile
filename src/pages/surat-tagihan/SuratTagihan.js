import React from 'react';
import { View, ScrollView} from 'react-native';
import { CardList } from '../../components';
import mystyles from '../../config/mystyles';

export default class SuratTagihan extends React.Component{
  render(){
    return(
      <View style={[mystyles.page]}>
        <View style={[mystyles.page, mystyles.bgBlue]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>

            <CardList
              title="PT. Nemo Nusantara"
              category="2017"
              label="50.000.000" 
              icon={ require('../../../assets/icons/wallet.png') }
              data={[
                {label:'Nomor Surat', value:'424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018'},
              ]}
              page={ () => this.props.navigation.navigate('suratTagihanDetail') } />

            <CardList
              title="PT. Nemo Nusantara"
              category="2017"
              label="50.000.000" 
              icon={ require('../../../assets/icons/wallet.png') }
              data={[
                {label:'Nomor Surat', value:'424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018'},
              ]}
              page={ () => this.props.navigation.navigate('suratTagihanDetail') } />

            <CardList
              title="PT. Nemo Nusantara"
              category="2017"
              label="50.000.000" 
              icon={ require('../../../assets/icons/wallet.png') }
              data={[
                {label:'Nomor Surat', value:'424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018'},
              ]}
              page={ () => this.props.navigation.navigate('suratTagihanDetail') } />
              
          </ScrollView>
        </View>
      </View>
    );
  }
}