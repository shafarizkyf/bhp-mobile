import React from 'react';
import { View, ScrollView, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { MyTimeline, CardSurat } from '../../components';
import mystyles from '../../config/mystyles';

const data = [
  {time: '01/11/2018', title: 'Staff Pencegahan dan Penertiban', description: 'Bambang Wijayanto'},
  {time: '02/11/2018', title: 'Kasi Pencegahan dan Penertiban', description: 'Nana Wiyanti'},
  {time: '03/11/2018', title: 'Kasubit Pencegahan dan Penertiban', description: 'Chista Ella'},
  {time: '04/11/2018', title: 'Direktur Pengendalian POS dan Informatika', description: 'Tarko'},
];

export default class SuratTagihanDetail extends React.Component{
  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <View style={[mystyles.mh20, mystyles.page]}>
          <View style={[mystyles.mt20]}></View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <CardSurat label="Nomor Surat" value="KOMINFO/DJPPI.6.5/P1.05.05/2018" />
            <MyTimeline data={data} />
          </ScrollView>
        </View>
        <TouchableOpacity>
          <Text style={[mystyles.validasi]}>VALIDASI</Text>
        </TouchableOpacity>
      </View>
    );
  }
}