import React from 'react';
import {
  View,
  ScrollView,
  Text, 
  StyleSheet,
  Dimensions
} from 'react-native';
import { Navbar } from '../../components';
import {
  LineChart,
  PieChart,
} from 'react-native-chart-kit'
import mystyles from '../../config/mystyles';
import colors from '../../config/colors';

const lineChartConfig = {
  backgroundColor: '#e26a00',
  backgroundGradientFrom: '#76DDFB',
  backgroundGradientTo: '#2C82BE',
  decimalPlaces: 0,
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
}

export default class Dashboard extends React.Component{

  state = {
    lineChartData: {
      labels: ['2018', '2017', '2016', '2015', '2014'],
      datasets: [{
        data: [
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
        ]
      }]
    },
    wajibBayarPembayaranChartData:[
      { name: 'Sudah Bayar', population: 100, color: '#76DDFB', legendFontColor: '#7F7F7F', legendFontSize: 12 },
      { name: 'Belum Bayar', population: 30, color: '#2C82BE', legendFontColor: '#7F7F7F', legendFontSize: 12 },
    ],
    pelaporanDokumenDataChart:[
      { name: 'Sudah Lapor', population: 60, color: '#76DDFB', legendFontColor: '#7F7F7F', legendFontSize: 12 },
      { name: 'Belum Lapor', population: 40, color: '#2C82BE', legendFontColor: '#7F7F7F', legendFontSize: 12 },
    ],
    wajibBayarCoklitDataChart:[
      { name: 'Coklit Selesai', population: 30, color: '#76DDFB', legendFontColor: '#7F7F7F', legendFontSize: 12 },
      { name: 'Belum Coklit', population: 70, color: '#2C82BE', legendFontColor: '#7F7F7F', legendFontSize: 12 },      
    ],
    statusWajibBayarCoklitDataChart:[
      { name: 'Aktif', population: 60, color: '#76DDFB', legendFontColor: '#7F7F7F', legendFontSize: 12 },
      { name: 'Tidak Aktif', population: 10, color: '#2C82BE', legendFontColor: '#7F7F7F', legendFontSize: 12 },            
      { name: 'Rek. Cabut Izin', population: 5, color: '#DBECF8', legendFontColor: '#7F7F7F', legendFontSize: 12 },            
    ]
  }

  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } title="Dashboard" />

        <ScrollView style={[]} showsVerticalScrollIndicator={false}>
          <View style={[mystyles.page, mystyles.mh20]}>

            <View style={[mystyles.mt10]}></View>
            <View style={[styles.card]}>
              <Text style={[styles.title]}>Penerimaan Negara Bukan Pajak</Text>
              <LineChart
                data={ this.state.lineChartData }
                width={ Dimensions.get('window').width - 40}
                chartConfig={ lineChartConfig }
                height={ 180 }
                bezier 
              />
            </View>

            <View style={[styles.card]}>
              <Text style={[styles.title]}>Wajib Bayar Pembayaran</Text>
              <PieChart
                data={ this.state.wajibBayarPembayaranChartData }
                width={ Dimensions.get('window').width - 40}
                height={160}
                chartConfig={{ color: () => 'rgba(255, 255, 255, 1)'}}
                accessor="population"
                backgroundColor="#fff"
              />
            </View>

            <View style={[styles.card]}>
              <Text style={[styles.title]}>Pelaporan Dokumen</Text>
              <PieChart
                data={ this.state.pelaporanDokumenDataChart }
                width={ Dimensions.get('window').width - 40}
                height={160}
                chartConfig={{ color: () => 'rgba(255, 255, 255, 1)' }}
                accessor="population"
                backgroundColor="#fff"
              />
            </View>

            <View style={[styles.card]}>
              <Text style={[styles.title]}>Wajib Bayar Coklit</Text>
              <PieChart
                data={ this.state.wajibBayarCoklitDataChart }
                width={ Dimensions.get('window').width - 40}
                height={160}
                chartConfig={{ color: () => 'rgba(255, 255, 255, 1)' }}
                accessor="population"
                backgroundColor="#fff"
              />
            </View>

            <View style={[styles.card]}>
              <Text style={[styles.title]}>Status Wajib Bayar</Text>
              <PieChart
                data={ this.state.statusWajibBayarCoklitDataChart }
                width={ Dimensions.get('window').width - 40}
                height={160}
                chartConfig={{ color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})` }}
                accessor="population"
                backgroundColor="#fff"
              />
            </View>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card:{
    marginBottom: 10,
    padding: 20,
    borderRadius: 10,
    backgroundColor: 'white',
    alignItems: 'center'
  },
  title:{
    fontSize: 16,
    textAlign: 'left',
    fontWeight: 'bold',
    color: colors.grey,
    marginBottom: 10
  }
});