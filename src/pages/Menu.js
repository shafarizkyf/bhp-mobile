import React from 'react';
import { View, ScrollView } from 'react-native';
import { CardMenu } from '../components';
import mystyles from '../config/mystyles';

export default class Menu extends React.Component{
  render(){
    return(
      <View style={[mystyles.page]}>
        <View style={[mystyles.bgBlue, mystyles.page]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>
            <CardMenu
              title="Dashboard" 
              icon={ require('../../assets/icons/speedometer.png') }
              page={ () => this.props.navigation.navigate('dashboard') } />

            <CardMenu
              title="Dokumen" 
              icon={ require('../../assets/icons/book.png') }
              page={ () => this.props.navigation.navigate('verifikasiDokumen') } />

            <CardMenu
              title="Coklit" 
              icon={ require('../../assets/icons/maths.png') }
              page={ () => this.props.navigation.navigate('coklit') } />

            <CardMenu
              title="Laporan" 
              icon={ require('../../assets/icons/graph.png') }
              page={ () => this.props.navigation.navigate('laporan') } />

            <CardMenu
              title="Profile" 
              icon={ require('../../assets/icons/worker.png') }
              page={ () => this.props.navigation.navigate('profile') } />
          </ScrollView>
        </View>
      </View>
    );
  }
}