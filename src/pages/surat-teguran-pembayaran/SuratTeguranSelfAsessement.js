import React from 'react';
import { View, ScrollView } from 'react-native';
import { CardTransactionList } from '../../components';
import mystyles from '../../config/mystyles';

export default class SuratTeguranSelfAsessement extends React.Component{
  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mt20]}></View>

          <CardTransactionList 
            icon={ require('../../../assets/icons/money.png') }
            date1="30 April 2018"
            subject="BHP TEL"
            amount="90.000.000"
            amountTitle="Tagihan"
            dataFooter={[
              {label:'Terbayar', value:'0'},
              {label:'Denda', value:'0'},
              {label:'Kurang Bayar', value:'0'}]} 
            />

          <CardTransactionList 
            icon={ require('../../../assets/icons/money.png') }
            date1="30 April 2018"
            subject="KPU"
            amount="180.000.000"
            amountTitle="Tagihan"
            dataFooter={[
              {label:'Terbayar', value:'0'},
              {label:'Denda', value:'0'},
              {label:'Kurang Bayar', value:'0'}]} 
            />

        </ScrollView>
      </View>
    );
  }
}