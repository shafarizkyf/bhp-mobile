import React from 'react';
import { View, ScrollView} from 'react-native';
import { CardList } from '../../components';
import mystyles from '../../config/mystyles';

export default class SuratTeguranDokumen extends React.Component{
  render(){
    return(
      <View style={[mystyles.page]}>
        <View style={[mystyles.page, mystyles.bgBlue]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>

            <CardList
              title="PT. Karya Sumampir"
              category="2017"
              label="Teguran III" 
              icon={ require('../../../assets/icons/postal.png') }
              data={[
                {label:'Nomor Surat', value:'424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018'},
              ]}
              page={() => this.props.navigation.navigate('suratTeguranDokumenDetailAll')} />

            <CardList
              title="PT. Karya Sumampir"
              category="2017"
              label="Teguran III" 
              icon={ require('../../../assets/icons/postal.png') }
              data={[
                {label:'Nomor Surat', value:'424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018'},
              ]}
              page={() => this.props.navigation.navigate('suratTeguranDokumenDetailAll')} />

            <CardList
              title="PT. Karya Sumampir"
              category="2017"
              label="Teguran III" 
              icon={ require('../../../assets/icons/postal.png') }
              data={[
                {label:'Nomor Surat', value:'424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018'},
              ]}
              page={() => this.props.navigation.navigate('suratTeguranDokumenDetailAll')} />
              
          </ScrollView>
        </View>
      </View>
    );
  }
}