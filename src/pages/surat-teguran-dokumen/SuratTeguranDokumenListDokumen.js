import React from 'react';
import { View, ScrollView } from 'react-native';
import { CardSurat } from '../../components';
import mystyles from '../../config/mystyles';

export default class SuratTeguranDokumenListDokumen extends React.Component{
  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <View style={[mystyles.mh20, mystyles.page]}>
          <View style={[mystyles.mt20]}></View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <CardSurat label="Dokumen" status="Terima" value="Laporan Keuangan" />
            <CardSurat label="Dokumen" status="Terima" value="Daftar Akun" />
            <CardSurat label="Dokumen" status="Tolak" value="Buku Besar" />
            <CardSurat label="Dokumen" status="Terima" value="Neraca Percobaan" />
            <CardSurat label="Dokumen" status="Terima" value="Dasar Dokumen Perhitungan" />
            <CardSurat label="Dokumen" status="Terima" value="Kumpulan Bukti Pembayaran" />
          </ScrollView>
        </View>
      </View>
    );
  }
}