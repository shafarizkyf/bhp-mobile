import Dashboard from './dashboard/Dashboard';
import Laporan from './laporan/Laporan';
import LaporanPiutang from './laporan/LaporanPiutang';
import LaporanPiutangDenda from './laporan-piutang/LaporanPiutangDenda';
import LaporanPiutangPnbp from './laporan-piutang/LaporanPiutangPnbp';
import LaporanTagihan from './laporan/LaporanTagihan';
import LaporanTagihanDetail from './laporan-tagihan/LaporanTagihanDetail';
import LaporanTagihanCoklit from './laporan-tagihan/LaporanTagihanCoklit';
import LaporanWajibBayar from './laporan/LaporanWajibBayar';
import Coklit from './coklit/Coklit';
import PerhitunganCoklit from './coklit/PerhitunganCoklit';
import KesepakatanCoklit from './coklit/KesepakatanCoklit';
import TagihanCoklit from './coklit/TagihanCoklit';
import Surat from './Surat';
import SuratHasilCoklit from './surat-hasil-coklit/SuratHasilCoklit';
import SuratHasilCoklitDetail from './surat-hasil-coklit/SuratHasilCoklitDetail';
import SuratPemberitahuan from './surat-pemberitahuan/SuratPemberitahuan';
import SuratPemberitahuanDetail from './surat-pemberitahuan/SuratPemberitahuanDetail';
import SuratTagihan from './surat-tagihan/SuratTagihan';
import SuratTagihanDetail from './surat-tagihan/SuratTagihanDetail';
import SuratTeguranDokumen from './surat-teguran-dokumen/SuratTeguranDokumen';
import SuratTeguranDokumenListDokumen from './surat-teguran-dokumen/SuratTeguranDokumenListDokumen';
import SuratTeguranDokumenDetail from './surat-teguran-dokumen/SuratTeguranDokumenDetail';
import SuratTeguranPembayaran from './surat-teguran-pembayaran/SuratTeguranPembayaran';
import SuratTeguranSelfAsessement from './surat-teguran-pembayaran/SuratTeguranSelfAsessement';
import SuratTeguranPembayaranDetail from './surat-teguran-pembayaran/SuratTeguranPembayaranDetail';
import VerifikasiDokumen from './verifikasi-dokumen/VerifikasiDokumen';
import VerifikasiDokumenDetail from './verifikasi-dokumen/VerifikasiDokumenDetail';
import PreviewDokumen from './verifikasi-dokumen/PreviewDokumen';
import Profile from './profile/Profile';
import Menu from './Menu';

export {
  Dashboard,
  Laporan,
  LaporanPiutang,
  LaporanTagihan,
  LaporanWajibBayar,
  LaporanPiutangDenda,
  LaporanPiutangPnbp,
  LaporanTagihanDetail,
  LaporanTagihanCoklit,
  Coklit,
  PerhitunganCoklit,
  KesepakatanCoklit,
  TagihanCoklit,
  Surat,
  SuratHasilCoklit,
  SuratHasilCoklitDetail,
  SuratPemberitahuan,
  SuratPemberitahuanDetail,
  SuratTagihan,
  SuratTagihanDetail,
  SuratTeguranDokumen,
  SuratTeguranDokumenDetail,
  SuratTeguranPembayaran,
  SuratTeguranPembayaranDetail,
  VerifikasiDokumen,
  VerifikasiDokumenDetail,
  PreviewDokumen,
  SuratTeguranSelfAsessement,
  SuratTeguranDokumenListDokumen,
  Profile,
  Menu
}