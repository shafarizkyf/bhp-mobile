import React from 'react';
import { View, ScrollView } from 'react-native';
import { Navbar, CardMenu } from '../../components';
import mystyles from '../../config/mystyles';

export default class Laporan extends React.Component{
  render(){
    return(
      <View style={[mystyles.page]}>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } title="Laporan" />
        <View style={[mystyles.bgBlue, mystyles.page]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>
            <CardMenu
              title="Laporan Piutang" 
              icon={ require('../../../assets/icons/credit-card.png') }
              page={ () => this.props.navigation.navigate('laporanPiutang') } />

            <CardMenu
              title="Laporan Tagihan" 
              icon={ require('../../../assets/icons/contract.png') }
              page={ () => this.props.navigation.navigate('laporanTagihan') } />

            <CardMenu
              title="Laporan Wajib Bayar" 
              icon={ require('../../../assets/icons/building.png') }
              page={ () => this.props.navigation.navigate('laporanWajibBayar') } />
          </ScrollView>
        </View>
      </View>
    );
  }
}