import React from 'react';
import { View, ScrollView} from 'react-native';
import { CardList } from '../../components';
import mystyles from '../../config/mystyles';

export default class LaporanWajibBayar extends React.Component{

  state = {
    wajibBayar: [
      {
        nama: "PT. REKSA KARYA",
        selfAsessement: "20.500.000.000",
        penetapan: "20.500.000.000",
        piutang: "20.000.000",
        status: "lengkap",
        tahun: "2018"
      },
      {
        nama: "PT. TELKOM",
        selfAsessement: "20.500.000.000",
        penetapan: "20.500.000.000",
        piutang: "20.000.000",
        status: "lengkap",
        tahun: "2018"
      },
      {
        nama: "PT. INDOSAT",
        selfAsessement: "20.000.000.000",
        penetapan: "20.000.000.000",
        piutang: "20.000.000",
        status: "sepakat",
        tahun: "2018"
      }
    ]
  }

  render(){
    return(
      <View style={[mystyles.page]}>
        <View style={[mystyles.page, mystyles.bgBlue]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>
            {
              this.state.wajibBayar.map((obj, index) => (
                <CardList
                  key={ index }
                  title={ obj.nama }
                  category={ obj.tahun }
                  label={ obj.status.toUpperCase() } 
                  icon={ require('../../../assets/icons/building.png') }
                  data={[
                    { label:'Self Asessement', value:obj.selfAsessement },
                    { label:'Penetapan', value:obj.penetapan },
                  ]}
                />
              ))
            }              
          </ScrollView>
        </View>
      </View>
    );
  }
}