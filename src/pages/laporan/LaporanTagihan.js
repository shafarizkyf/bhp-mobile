import React from 'react';
import { View, ScrollView} from 'react-native';
import { CardList } from '../../components';
import mystyles from '../../config/mystyles';

export default class LaporanTagihan extends React.Component{

  state = {
    wajibBayar:[
      {
        nama: "PT. TELKOM",
        jenisTagihan: "BHP TEL",
        tagihan: "100.000.000",
        terbayar: "90.000.000",
        piutang: '11.040.080',
        tahunBuku: "2017",
        jatuhTempo: "30 April 2018",
      },
      {
        nama: "PT. TELKOM",
        jenisTagihan: "KPU",
        tagihan: "200.000.000",
        terbayar: "170.000.000",
        piutang: '33.122.424',
        tahunBuku: "2017",
        jatuhTempo: "30 April 2018",
      },
      {
        nama: "PT. REKSA KARYA",
        jenisTagihan: "BHP TEL",
        tagihan: "120.000.000",
        terbayar: "120.000.000",
        piutang: '0',
        tahunBuku: "2017",
        jatuhTempo: "30 April 2018",
      },
      {
        nama: "PT. REKSA KARYA",
        jenisTagihan: "KPU",
        tagihan: "120.000.000",
        terbayar: "120.000.000",
        piutang: '0',
        tahunBuku: "2017",
        jatuhTempo: "30 April 2018",
      },
    ]
  }

  render(){
    return(
      <View style={[mystyles.page]}>
        <View style={[mystyles.page, mystyles.bgBlue]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>
            {
              this.state.wajibBayar.map((obj, index) => (
                <CardList
                  key={index}
                  title={ obj.nama }
                  category={ obj.jenisTagihan }
                  label={ obj.tagihan }
                  data={[
                    { label:'Terbayar', value:obj.terbayar },
                    { label:'Piutang', value:obj.piutang },
                    { label:'Jatuh Tempo', value:obj.jatuhTempo },
                  ]}
                  icon={ require('../../../assets/icons/contract.png') }
                  page={ () => this.props.navigation.navigate('laporanTagihanDetail') 
                } />
              ))
            }
          </ScrollView>
        </View>
      </View>
    );
  }
}