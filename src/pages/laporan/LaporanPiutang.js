import React from 'react';
import { View, ScrollView} from 'react-native';
import { CardList } from '../../components';
import mystyles from '../../config/mystyles';

export default class LaporanPiutang extends React.Component{

  state =  {
    data:[
      {
        wajibBayar: 'PT. TELKOM',
        jenisTagihan: 'KPU',
        totalPiutang: '11.040.080',
        terbayar: '0',
        denda: '1.040.080',
        jatuhTempo: '30 September 2018'
      },
      {
        wajibBayar: 'PT. TELKOM',
        jenisTagihan: 'BPH TEL',
        totalPiutang: '33.122.424',
        terbayar: '0',
        denda: '3.122.424',
        jatuhTempo: '30 September 2018'
      },
    ]
  }

  render(){
    return(
      <View style={[mystyles.page]}>
        <View style={[mystyles.page, mystyles.bgBlue]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>

            {
              this.state.data.map((obj, index) => (
                <CardList
                  key={index}
                  title={ obj.wajibBayar }
                  category={ obj.jenisTagihan }
                  label={ obj.totalPiutang } 
                  data={[
                    { label:'Terbayar', value:obj.terbayar },
                    { label:'Denda', value:obj.denda },
                    { label:'Jatuh Tempo', value:obj.jatuhTempo },
                  ]}
                  icon={ require('../../../assets/icons/credit-card.png') }
                  page={ () => this.props.navigation.navigate('laporanPiutangDetail') } 
                />
              ))
            }
              
          </ScrollView>
        </View>
      </View>
    );
  }
}