import React from 'react';
import { View, ScrollView} from 'react-native';
import { CardList } from '../../components';
import mystyles from '../../config/mystyles';

export default class SuratPemberitahuan extends React.Component{

  state = {
    data : [
      {
        wajibBayar: "PT. INDOSAT",
        tahun: "2017",
        nomorSurat: "	424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018",
        biaya: "18.000.000.000" 
      },
      {
        wajibBayar: "PT. 3 (TIGA) TEL",
        tahun: "2016",
        nomorSurat: "	424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018",
        biaya: "20.000.000.000" 
      },
      {
        wajibBayar: "PT. XL",
        tahun: "2016",
        nomorSurat: "	424/KOMINFO/DJPPI.6.5/PI.05.05/03/2018",
        biaya: "10.000.000.000" 
      },
    ]
  }

  render(){
    return(
      <View style={[mystyles.page]}>
        <View style={[mystyles.page, mystyles.bgBlue]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt20]}></View>
            {
              this.state.data.map((obj, index) => (
                <CardList
                  key={ index }
                  title={ obj.wajibBayar }
                  label={ obj.biaya }
                  category={ obj.tahun }
                  data={[
                    { label:'Nomor Surat', value:obj.nomorSurat },
                  ]}
                  icon={ require('../../../assets/icons/clipboard.png') }
                  page={ () => this.props.navigation.navigate('suratPemberitahuanDetail') } 
                />
              ))
            }              
          </ScrollView>
        </View>
      </View>
    );
  }
}