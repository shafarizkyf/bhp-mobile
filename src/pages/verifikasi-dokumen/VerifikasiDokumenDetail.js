import React from 'react';
import { 
  View, 
  ScrollView, 
  Modal, 
  TextInput, 
  StyleSheet, 
  Text,
  TouchableOpacity,
  PermissionsAndroid
} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import Snackbar from 'react-native-snackbar';
import { CardSurat } from '../../components';
import mystyles from '../../config/mystyles';
import colors from '../../config/colors';

export default class VerifikasiDokumenDetail extends React.Component{

  state = {
    isModalVisible: false,
    dokumen:[
      {
        nama: 'Laporan Keuangan',
        link: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        statusVerifikasi: null,
        memo: null,
      },
      {
        nama: 'Daftar Akun',
        link: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        statusVerifikasi: null,
        memo: null,
      },
      {
        nama: 'Buku Besar',
        link: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        statusVerifikasi: null,
        memo: null,
      },
      {
        nama: 'Neraca Percobaan',
        link: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        statusVerifikasi: null,
        memo: null,
      },
      {
        nama: 'Dokumen Dasar Perhitungan',
        link: 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        statusVerifikasi: null,
        memo: null,
      },
      {
        nama: 'Kumpulan Bukti Pembayaran',
        link: null,
        statusVerifikasi: null,
        memo: null,
      },
    ]
  }

  preview(index){
    let pdf = this.state.dokumen[index].link;
    this.props.navigation.navigate('previewDokumen',{pdf});
  }

  async download(index){

    const permission = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
    );
    
    if(permission === PermissionsAndroid.RESULTS.GRANTED){
      let dirs = RNFetchBlob.fs.dirs;
  
      RNFetchBlob
      .config({
        addAndroidDownloads : {
          useDownloadManager : true,
          notification : true,
          path:  dirs.DownloadDir + '.pdf',
          description : 'Download Dokumen BHP.',
          mime: 'application/pdf'
        }
      })
      .fetch('GET', this.state.dokumen[index].link, {
        //some headers ..
      })
      .then((res) => {
        Snackbar.show({
          title: 'Dokumen berhasil disimpan di folder Download',
          duration: Snackbar.LENGTH_LONG,
          backgroundColor: colors.orange
        });
      });
    }
  }

  setMemo(){
    let dokumen = [...this.state.dokumen];
    dokumen[this.state.selectedDokumenIndex].memo = this.state.currentMemo;
    this.setState({isModalVisible:false, dokumen});
    console.log('current dokumen data -->', dokumen);
  }

  onVerification(status, index){
    //menamilkan input memo saat dokumen ditolak
    const isModalVisible = status ? false : true;

    let dokumen = [...this.state.dokumen];
    dokumen[index].statusVerifikasi = status;

    selectedDokumenIndex = index;
    this.setState({isModalVisible, dokumen, selectedDokumenIndex});
  }

  verifikasiDokumen(){
    alert('do something');
  }

  render(){
    return(
      <View style={[mystyles.page]}>
        <View style={[mystyles.page, mystyles.bgBlue]}>
          <Modal
            transparent={true}
            animationType="slide"
            visible={this.state.isModalVisible}
            onRequestClose={() => this.setState({isModalVisible:false})}>

            <View style={[styles.memoModal]}>
              <View style={[styles.card]}>
                <Text style={[mystyles.f12, mystyles.mb10, mystyles.textGrey]}>Memo Penolakan</Text>
                <TextInput 
                  style={[styles.textArea]}
                  multiline={true}
                  numberOfLines={4}
                  placeholder="Memo"
                  onChangeText={(currentMemo)=>this.setState({currentMemo})} />

                <TouchableOpacity activeOpacity={0.8} onPress={()=>this.setMemo()}>
                  <Text style={[styles.button]}>Simpan</Text>
                </TouchableOpacity>
              </View>
            </View>

          </Modal>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>
            {
              this.state.dokumen.map((obj, index) => (
                <CardSurat
                  key={index}
                  index={index}
                  label="Nama Dokumen"
                  status={obj.statusVerifikasi === null ? 'Belum Ada Tindakan' : obj.statusVerifikasi ? 'Terima' : 'Tolak' }
                  value={obj.nama}
                  verification
                  onVerification={(status, index) => this.onVerification(status, index)}
                  preview={(index) => this.preview(index)}
                  download={(index) => this.download(index)}
                />
              ))
            }
          </ScrollView>
          <TouchableOpacity activeOpacity={0.8} onPress={()=>this.verifikasiDokumen()}>
            <Text style={[styles.buttonAction]}>VERIFIKASI</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  memoModal:{
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.8)',
    flex:1
  },
  card:{
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 3,
    marginHorizontal: 20,
    marginTop: 100
  },
  textArea:{
    height: 150,
    padding: 10,
    textAlignVertical: 'top',
    borderWidth: 1,
    borderColor: colors.lightGrey,
    backgroundColor: 'white',
    borderRadius: 3
  },
  button:{
    marginTop:20,
    backgroundColor: colors.orange,
    textAlign: 'center',
    paddingVertical: 10,
    borderRadius: 3,
    color: 'white',
    fontWeight: 'bold'
  },
  buttonAction:{
    backgroundColor: colors.softBlue,
    paddingVertical: 15,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white',
  }
})