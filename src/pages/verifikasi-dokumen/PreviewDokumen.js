import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import Pdf from 'react-native-pdf';

export default class PreviewDokumen extends React.Component{

  state={
    source: null
  }

  componentDidMount(){
    let pdfSource = this.props.navigation.getParam('pdf');
    this.setState({source:{uri:pdfSource}});
  }

  render(){
    return(
      <View style={styles.container}>
        {
          this.state.source &&
          <Pdf 
            source={this.state.source} 
            style={styles.pdf} 
            onError={(error) => console.log('error read pdf -->', error)}
          />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  pdf:{
    flex:1,
    width:Dimensions.get('window').width,
  }
});
