import React from 'react';
import { View, ScrollView } from 'react-native';
import { CardTransactionList } from '../../components';
import mystyles from '../../config/mystyles';

export default class LaporanPiutangDenda extends React.Component{

  state = {
    rincianDenda: [
      {
        jenisPembayaran: 'BHP TEL',
        tanggal: '30 April 2018',
        denda: '0',
        akumulasi: '0',
        terhutang: '10.000.000'
      },
      {
        jenisPembayaran: 'BHP TEL',
        tanggal: 'Mei 2018',
        denda: '200.000',
        akumulasi: '200.000',
        terhutang: '10.200.000'
      },
      {
        jenisPembayaran: 'BHP TEL',
        tanggal: 'Juni 2018',
        denda: '204.000',
        akumulasi: '404.000',
        terhutang: '10.404.000'
      },
      {
        jenisPembayaran: 'BHP TEL',
        tanggal: 'Juli 2018',
        denda: '208.000',
        akumulasi: '612.808',
        terhutang: '10.612.000'
      },
      {
        jenisPembayaran: 'BHP TEL',
        tanggal: 'Agustus 2018',
        denda: '212.424',
        akumulasi: '824.322',
        terhutang: '10.324.322'
      },
      {
        jenisPembayaran: 'BHP TEL',
        tanggal: 'September 2018',
        denda: '216.486',
        akumulasi: '1.040.808',
        terhutang: '11.040.808'
      },
    ],
  }

  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mt20]}></View>
          {
            this.state.rincianDenda.map((obj, index) => (
              <CardTransactionList 
                key={index}
                icon={ require('../../../assets/icons/money.png') }
                date1={ obj.tanggal }
                subject={ obj.jenisPembayaran }
                amount={ obj.terhutang }
                amountTitle="Terhutang"
                dataFooter={[
                  { label:'Perhitungan Denda', value:obj.denda },
                  { label:'Akumulasi Denda', value:obj.terhutang }]} 
              />
            ))
          }
        </ScrollView>
      </View>
    );
  }
}