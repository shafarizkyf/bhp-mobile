import React from 'react';
import { View, ScrollView } from 'react-native';
import { CardCompanyOverview, CardTransactionList } from '../../components';
import mystyles from '../../config/mystyles';

export default class LaporanPiutangPnbp extends React.Component{

  state = {
    wajibBayar:{
      nama: 'PT. TELKOM',
      jenisTagihan: 'KPU',
      jumlahPiutang: '11.040.080',
      terbayar: '0',
      penetapan: '03 September 2018',
      jatuhTempo: '30 September 2018'
    },
    rincianPembayaran:[
      {
        waktuPembayaran: '1 Oktober 2018 11:00',
        kode: 'AXS001BWS',
        jumlah: '5.000.000',
        tanggalAlokasi: '10 Oktober 2018'
      }
    ]
  }

  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mt20]}></View>
          
          <CardCompanyOverview 
            title={ this.state.wajibBayar.nama }
            dataBody={[
              { label:'Jumlah Piutang', value:this.state.wajibBayar.jumlahPiutang },
              { label:'Terbayar', value:this.state.wajibBayar.terbayar },
              { label:'Jenis', value:this.state.wajibBayar.jenisTagihan }]}
            dataFooter={[
              { label:'Tanggal Penetapan', value:this.state.wajibBayar.penetapan },
              { label:'Jatuh Tempo', value:this.state.wajibBayar.jatuhTempo }]} 
          />
          
          {
            this.state.rincianPembayaran.map((obj, index) => (
              <CardTransactionList
                key={index}
                icon={ require('../../../assets/icons/tax.png') }
                date1={ obj.waktuPembayaran }
                date2={ `Alokasi ${obj.tanggalAlokasi}` }
                subject={ obj.kode }
                amount={ obj.jumlah } 
              />
            ))
          }

        </ScrollView>
      </View>
    );
  }
}