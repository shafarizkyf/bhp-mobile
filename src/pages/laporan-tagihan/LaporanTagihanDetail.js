import React from 'react';
import { View, ScrollView } from 'react-native';
import { CardCompanyOverview, CardTransactionList } from '../../components';
import mystyles from '../../config/mystyles';

export default class LaporanTagihanDetail extends React.Component{

  state = {
    wajibBayar:{
      nama: "PT. TELKOM",
      jenisTagihan: "BHP TEL",
      tagihan: "100.000.000",
      terbayar: "90.000.000",
      piutang: '11.040.080',
      denda: '1.040.080',
      tahunBuku: "2017",
      jatuhTempo: "30 April 2018",
    },
    rincianPembayaran:[
      {
        waktuPembayaran: "1 Januari 2018 03:00",
        kode: "001/KSHHA/001",
        jumlah: "50.000.000",
        alokasi: "1 Januari 2018 05:00"
      },
      {
        waktuPembayaran: "15 Januari 2018 03:00",
        kode: "001/KSHHA/002",
        jumlah: "40.000.000",
        alokasi: "15 Januari 2018 05:00"
      },
    ]
  }

  render(){
    return(
      <View style={[mystyles.page, mystyles.bgBlue]}>
        <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mt20]}></View>
          <CardCompanyOverview 
            title={ this.state.wajibBayar.nama }
            dataBody={[
              { label:'Jumlah Tagihan', value:this.state.wajibBayar.tagihan },
              { label:'Terbayar', value:this.state.wajibBayar.terbayar },
              { label:'Jenis', value:this.state.wajibBayar.jenisTagihan }
            ]}
            dataFooter={[
              { label:'Denda', value:this.state.wajibBayar.denda },
              { label:'Piutang', value:this.state.wajibBayar.piutang },
              { label:'Jatuh Tempo', value:this.state.wajibBayar.jatuhTempo }
            ]} 
          />
          
          {
            this.state.rincianPembayaran.map((obj, index) => (
              <CardTransactionList
                key={ index }
                icon={ require('../../../assets/icons/money.png') }
                date1={ obj.waktuPembayaran }
                date2={ `Alokasi ${obj.alokasi}` }
                subject={ obj.kode }
                amount={ obj.jumlah } 
              />  
            ))
          }
        </ScrollView>
      </View>
    );
  }
}