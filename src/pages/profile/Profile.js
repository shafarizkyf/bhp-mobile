import React from 'react';
import {
  View, 
  Text, 
  StyleSheet, 
  Image, 
  ImageBackground, 
  TouchableOpacity, 
  Platform,
  Modal,
  TextInput
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import SignatureCapture from 'react-native-signature-capture';
import mystyles from '../../config/mystyles';
import colors from '../../config/colors';

export default class Profile extends React.Component{

  state = {
    image: null,
    isModalVisible: false,
    signature: null
  }

  changeProfilPicture(){
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: Platform.Version === 27 && Platform.OS === "android" ? true : false
    }).then(image => {
      this.setState({image: image.path});
      console.log('change profil picture -->', image);
    });
  }

  async updateProfileData(){
    this.setState({isModalVisible:false});
  }

  saveSign() {
    this.refs["sign"].saveImage();
  }

  resetSign() {
    this.refs["sign"].resetImage();
  }

  _onSaveEvent(result) {
    console.log('your signature -->', result);
    const base64 = `data:image/png;base64,${result.encoded}`;    
    this.setState({signature: {base64, path: result.pathName}});
  }

  _onDragEvent() {
    // This callback will be called when the user enters signature
    console.log("dragged");
  }

  render(){
    return (
      <View style={[mystyles.page, mystyles.justifyCenter]}>
        <ImageBackground source={ require('../../../assets/images/bg2.jpg') } style={mystyles.loginBg} />
        <View style={mystyles.loginBgColor}></View>
        
        <Modal 
          animationType="slide"
          visible={ this.state.isModalVisible }
          onRequestClose={ () => this.setState({isModalVisible:false}) } >
          <View style={[mystyles.page, mystyles.mh20]}>
            <Text style={[mystyles.textCenter, mystyles.mb20, mystyles.f18, mystyles.mt20]}>Ubah Profile</Text>
            <TextInput placeholder="username" style={[styles.textInput]} />
            <TextInput placeholder="password" style={[styles.textInput]} />

            <View style={[styles.signatureWrapper]}>
              <Text style={[mystyles.f12, mystyles.textCenter, mystyles.mb10]}>Tanda Tangan</Text>
              <SignatureCapture
                style={[mystyles.page, styles.signatureStroke]}
                ref="sign"
                onSaveEvent={(result) => this._onSaveEvent(result)}
                onDragEvent={() => this._onDragEvent()}
              />            
            </View>
          </View>
          <TouchableOpacity activeOpacity={0.8} onPress={ () => this.updateProfileData() }>
            <Text style={[styles.button]}>Simpan</Text>
          </TouchableOpacity>
        </Modal>

        <View style={[styles.card]}>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.changeProfilPicture() }>
              <Image source={ this.state.image ? {uri:this.state.image} : require('../../../assets/images/boy.png') } style={[styles.avatar]} />
            </TouchableOpacity>
            <Text style={[mystyles.bold, mystyles.f16]}>Shafa Rizky Fandestika</Text>
            <Text style={[mystyles.f14, mystyles.mb10]}>Divisi Teknologi Informasi</Text>
          </View>
          <TouchableOpacity activeOpacity={0.8} onPress={ () => this.setState({isModalVisible:true}) } >
            <Text style={[styles.button, styles.borderBottom]}>Ubah</Text>
          </TouchableOpacity>
        </View>

        <View style={[{alignItems:'center'}]}>
          <Text style={[mystyles.textWhite, mystyles.f12, mystyles.bold, mystyles.mb10]}>Your Signature</Text>
          <Image source={ this.state.signature ? {uri: this.state.signature.base64} : require('../../../assets/images/signature.png') } style={[styles.signature]} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  card:{
    backgroundColor: 'white',
    paddingTop: 20,
    borderRadius: 5,
    marginBottom: 20,
    marginHorizontal: 40
  },
  signature:{
    width: 100,
    height: 100,
  },
  signatureWrapper:{
    marginTop: 20,
    padding: 5,
    flex: 1,
    backgroundColor: colors.lightGrey,
    borderColor: colors.lightGrey,
    borderWidth: 1
  },
  signatureStroke: {
    flex: 1,
    borderColor: '#000033',
    borderWidth: 0.5,
  },
  avatar:{
    width: 100,
    height: 100,
    borderRadius: 50,
    marginBottom: 10
  },
  button:{
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: colors.orange,
    fontSize: 14,
    height: 40,
    paddingHorizontal: 30,
    marginTop: 20,
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  textInput:{
    fontSize: 16,
    paddingBottom: 10,
    color: colors.black,
    borderBottomWidth: 1,
    borderBottomColor: colors.black
  },
  borderBottom:{
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
  }
});