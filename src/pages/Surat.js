import React from 'react';
import { View, ScrollView } from 'react-native';
import { Navbar, CardMenu } from '../components';
import mystyles from '../config/mystyles';

export default class Surat extends React.Component{
  render(){
    return(
      <View style={[mystyles.page]}>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } title="Surat" />
        <View style={[mystyles.bgBlue, mystyles.page]}>
          <ScrollView style={[mystyles.mh20]} showsVerticalScrollIndicator={false}>
            <View style={[mystyles.mt10]}></View>
            <CardMenu
              title="Surat Pemberitahuan" 
              icon={ require('../../assets/icons/clipboard.png') }
              page={ () => this.props.navigation.navigate('suratPemberitahuan') } />

            <CardMenu
              title="Surat Teguran" 
              icon={ require('../../assets/icons/postal.png') }
              page={ () => this.props.navigation.navigate('suratTeguran') } />

            <CardMenu
              title="Surat Tagihan" 
              icon={ require('../../assets/icons/wallet.png') }
              page={ () => this.props.navigation.navigate('suratTagihan') } />

            <CardMenu
              title="Surat Hasil Coklit" 
              icon={ require('../../assets/icons/abacus.png') }
              page={ () => this.props.navigation.navigate('suratHasilCoklit') } />
          </ScrollView>
        </View>
      </View>
    );
  }
}