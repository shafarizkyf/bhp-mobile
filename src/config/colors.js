export default {
  softBlue: "#4BAAF4",
  blue: "#366CB1",
  navy: "#004FAC",
  orange: "#FC5836",
  grey: "#707070",
  lightGrey: "#E2E2E2",
  green: "#0BC24E",
  black: "#212121"
}