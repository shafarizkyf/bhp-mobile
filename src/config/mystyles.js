import { StyleSheet } from 'react-native';
import colors from '../config/colors';

export default StyleSheet.create({
  page:{
    flex:1
  },
  pageTitle:{
    fontSize: 20,
    color: colors.black,
    marginVertical: 10
  },
  row:{
    flexDirection: 'row',
    justifyContent: 'center'
  },
  circle:{
    borderRadius: 100,
  },
  card:{
    backgroundColor: 'white',
    borderRadius: 3,
    padding:10,
    marginBottom:5
  },
  border:{
    borderWidth: 1,
    borderColor: colors.grey
  },
  featureHeader: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingVertical: 10,
    paddingHorizontal: 10,
    color: colors.black
  },
  feature:{
    paddingHorizontal: 5,
    paddingVertical: 10,
    color: colors.black,
  },
  featureIcon:{
    height: 15,
    marginVertical: 15,
    alignSelf: 'center',
    resizeMode: 'contain'
  },
  borderGrey:{
    borderWidth: 1,
    borderColor: colors.grey,
  },
  tableLeft:{
    width: '45%',
    borderWidth: 1,
    borderColor: colors.grey,
  },
  tableRight:{
    width: '30%',
    borderWidth: 1,
    borderColor: colors.grey,
  },
  tableMiddle:{
    width: '25%',
    borderWidth: 1,
    borderColor: colors.grey,
    borderLeftWidth: 0,
    borderRightWidth: 0,
  },
  bgGrey: {
    backgroundColor: colors.grey
  },
  bgDarkGrey: {
    backgroundColor: 'grey'
  },
  bgLightGrey: {
    backgroundColor: colors.lightGrey
  },
  bgWhite: {
    backgroundColor: 'white'
  },
  bgLightBlue: {
    backgroundColor: colors.lightBlue
  },
  bgBlue: {
    backgroundColor: colors.blue
  },
  bgSoftBlue: {
    backgroundColor: colors.softBlue
  },
  bgLightGreen: {
    backgroundColor: colors.lightGreen
  },
  bgGreen: {
    backgroundColor: colors.green
  },
  bgRed: {
    backgroundColor: colors.red
  },
  bgOrange: {
    backgroundColor: colors.orange
  },
  bgYellow: {
    backgroundColor: colors.yellow
  },
  bgLightYellow: {
    backgroundColor: colors.lightYellow
  },
  textCenter:{
    textAlign: 'center'
  },
  textRight:{
    textAlign: 'right'
  },
  textLeft:{
    textAlign: 'left'
  },
  textGrey:{
    color: colors.grey
  },
  textTeal:{
    color: colors.teal
  },
  textGreen:{
    color: colors.green
  },
  textWhite:{
    color: 'white'
  },
  textRed:{
    color: colors.red
  },
  textYellow:{
    color: colors.yellow
  },
  textOrange:{
    color: colors.orange
  },
  textNavi:{
    color: colors.navy
  },
  textDarkGrey:{
    color: 'grey'
  },
  textLightGreen:{
    color: colors.lightGreen
  },
  show:{
    display: 'flex',
  },
  hide:{
    display: 'none',
  },
  mb0:{
    marginBottom: 0,
  },
  mb5:{
    marginBottom: 5,
  },
  mb10:{
    marginBottom: 10,
  },
  mb20:{
    marginBottom: 20,
  },
  p5:{
    padding:5
  },
  pv10:{
    paddingVertical: 10,
  },
  ph10:{
    paddingHorizontal: 10,
  },
  ph20:{
    paddingHorizontal: 20,
  },
  pt10:{
    paddingTop: 10,
  },
  pt20:{
    paddingTop: 20,
  },
  mt0:{
    marginTop: 0,
  },
  mt5:{
    marginTop: 5,
  },
  mt10:{
    marginTop: 10,
  },
  mt20:{
    marginTop: 20,
  },
  mh20:{
    marginHorizontal: 20,
  },
  ml5:{
    marginLeft: 5,
  },
  ml10:{
    marginLeft: 10,
  },
  ml20:{
    marginLeft: 20,
  },
  ml30:{
    marginLeft: 30,
  },
  ml40:{
    marginLeft: 40,
  },
  mr5:{
    marginRight: 5,
  },
  mr10:{
    marginRight: 10,
  },
  mr20:{
    marginRight: 20,
  },
  pb20:{
    paddingBottom: 20,
  },
  icon10:{
    height: 10,
    width: 10
  },
  icon20:{
    height: 20,
    width: 20
  },
  icon50:{
    height: 50,
    width: 50
  },
  iconFlag20:{
    height: 20,
    width: 30
  },
  border:{
    borderWidth:1, 
    borderColor:colors.black
  },
  f10:{
    fontSize: 10,
  },
  f12:{
    fontSize: 12,
  },
  f14:{
    fontSize: 14,
  },
  f16:{
    fontSize: 16,
  },
  f18:{
    fontSize: 18,
  },
  italic:{
    fontStyle: 'italic',
  },
  bold:{
    fontWeight:'bold'
  },
  row:{
    flexDirection: 'row',
  },
  column:{
    flexDirection: 'column',
  },
  pullRight:{
    alignSelf: 'flex-start',
  },
  pullLeft:{
    alignSelf: 'flex-end',
  },
  spaceBetween:{
    justifyContent: 'space-between'
  },
  crossMark15:{
    width: 15,
    height: 15
  },
  checkMark20:{
    width: 21,
    height: 20,
  },
  checkMark30:{
    width: 31.875,
    height: 30,
  },
  checkMark50:{
    width: 53.125,
    height: 50,
  },
  form:{
    margin:20,
  },
  justifyCenter:{
    justifyContent: 'center'
  },
  selfCenter:{
    alignSelf: 'center'
  },
  textInput:{
    borderWidth: 1,
    borderColor: colors.black,
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  formGroup:{
    marginVertical: 10,
  },
  formLabel:{
    color: colors.black,
    marginBottom: 5
  },
  formButton:{
    paddingVertical:10,
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    backgroundColor: colors.papaya,
  },
  alertSuccess:{
    backgroundColor: colors.lightGreen,
    borderColor: colors.green,
    borderWidth: 1,
    padding: 10,
    borderRadius: 2,
    marginBottom: 5,
  },
  formSectionWarning:{
    backgroundColor: colors.yellow,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    color: colors.brown
  },
  loginBg:{
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  menu:{
    backgroundColor: 'rgba(51, 111, 161, 0.7)',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 10,
    textAlign: 'center',
    paddingVertical: 18,
    color: 'white',
    marginBottom: 5,
    fontSize: 16,
  },
  menuSelected:{
    backgroundColor: 'rgba(255, 177, 8, 0.7)',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 10,
    textAlign: 'center',
    paddingVertical: 18,
    color: 'white',
    marginBottom: 5,
    fontSize: 16,
  },
  labelSend:{
    padding:5,
    backgroundColor: colors.navy,
    color: 'white',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    paddingHorizontal: 10
  },
  separator:{
    marginVertical: 10,
    borderWidth: 0.5,
    borderColor: colors.lightGrey
  },
  validasi:{
    backgroundColor: colors.softBlue,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    height: 50,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  loginBg:{
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  loginBgColor:{
    width: '100%',
    height: '100%',
    position: 'absolute',
    backgroundColor: 'rgba(0, 79, 172, 0.5)'
  },
});