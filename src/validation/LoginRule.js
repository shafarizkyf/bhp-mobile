const validate = {
  username:{
    presence: true
  },
  password:{
    presence: true
  }
}

export default validate;
