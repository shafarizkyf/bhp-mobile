const validate = {
  username:{
    presence: true
  },
  password:{
    presence: true
  },
  signature:{
    presence: {
      message: "^Your signature is required"
    },
    inclusion: {
      within: [true],
      message: "^Your signature is required"
    }  
  }
}

export default validate;